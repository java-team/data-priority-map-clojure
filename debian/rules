#!/usr/bin/make -f

include /usr/share/javahelper/java-vars.mk
include /usr/share/dpkg/pkg-info.mk

MDWN_DOCS = $(patsubst %.md,%.html,$(wildcard $(CURDIR)/*.md))

export CLASSPATH=/usr/share/java/clojure.jar

PRODUCED_JAR=data.priority-map.jar

%:
	dh $@ --with javahelper --with maven_repo_helper

override_jh_build: $(MDWN_DOCS)
	jar cf $(PRODUCED_JAR) -C src/main/clojure .
	mkdir -p $(CURDIR)/doc/html && mv $^ $(CURDIR)/doc/html

override_dh_auto_test:
	dh_auto_test
	(cd src/test/clojure && \
	  find . -name "test_*.clj" | xargs clojure -cp $(CURDIR)/$(PRODUCED_JAR))

override_jh_classpath:
	jh_classpath $(PRODUCED_JAR)

override_jh_installlibs:
	jh_installlibs $(PRODUCED_JAR)

override_dh_installdocs:
	dh_installdocs $(CURDIR)/*.md $(CURDIR)/doc/*

override_jh_clean:
	jh_clean
	rm -f $(CURDIR)/$(PRODUCED_JAR)
	rm -rf $(CURDIR)/doc

get-orig-source:
	uscan --download-version $(DEB_VERSION_UPSTREAM) --force-download --rename

%.html:%.md
	@echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"' \
		'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' > $@
	@echo "<html>" >> $@
	@echo "<head>" >> $@
	@echo "<title>$(shell head -n 1 $< | sed 's/^#*\s*//')</title>" >> $@
	@echo "</head>" >> $@
	@echo "<body>" >> $@
	markdown $< >> $@
	@echo "</body>" >> $@
	@echo "</html>" >> $@
